package com.test.controller;

import com.test.domain.Goods;
import com.test.domain.TradeOrder;
import com.test.domain.User;
import com.test.mapper.GoodsMapper;
import com.test.mapper.OrderMapper;
import com.test.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * @Description
 * @Date 2021/6/17
 */
@Slf4j
@RestController
public class TestController {

    @Resource(name = COMMON_BUSINESS_EXECUTOR)
    private Executor executor;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    public static final String COMMON_BUSINESS_EXECUTOR = "COMMON-BUSINESS-EXECUTOR-";

    /**
     * 通用 线程池
     */
    @Bean(COMMON_BUSINESS_EXECUTOR)
    public Executor commonBusinessExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 设置核心线程数
        executor.setCorePoolSize(Runtime.getRuntime().availableProcessors());
        // 设置最大线程数
        executor.setMaxPoolSize(Runtime.getRuntime().availableProcessors() * 2);
        // 配置队列大小
        executor.setQueueCapacity(10000);
        // 设置线程活跃时间（秒）
        executor.setKeepAliveSeconds(30);
        // 设置默认线程名称
        executor.setThreadNamePrefix(COMMON_BUSINESS_EXECUTOR);
        //拒绝策略选用 调用者执行策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        //执行初始化
        executor.initialize();
        return executor;
    }


    /**
     * 普通测试线程池接口
     */
    @GetMapping("thread")
    public Object thread(){
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            executor.execute(() -> {
                System.out.println(finalI);
            });
        }
        return 1;
    }

    /**
     * 普通测试线程池接口
     */
    @GetMapping("async1")
    public Object testasync1(){
        log.info("##################################  直接异步  ##################################");
        return async1();
    }

    @GetMapping("async2")
    public Object testasync2(){
        log.info("##################################  背压异步  ##################################");
        return async2();
    }

    @GetMapping("async3")
    public Object testasync3(){
        log.info("##################################  背压异步 - 简单版  ##################################");
        return async3(UUID.randomUUID().toString());
    }

    @GetMapping("sync")
    public Object testsync(){
        log.info("##################################  同步  ##################################");
        return sync();
    }

    public Object async1() {
        CountDownLatch latch = new CountDownLatch(3);
        executor.execute(() -> {
            userMapper.insert(new User());
            latch.countDown();
        });
        executor.execute(() -> {
            goodsMapper.insert(new Goods());
            latch.countDown();
        });
        executor.execute(() -> {
            orderMapper.insert(new TradeOrder());
            latch.countDown();
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public Object async2() {
        AsyncTransactionManager asyncTransactionManager = AsyncTransactionManager.builder()
                .transactionManager(transactionManager)
                .executor(executor)
                .build();
        return asyncTransactionManager
                .run(() -> userMapper.insert(new User()))
                .run(() -> goodsMapper.insert(new Goods()))
                .run(() -> orderMapper.insert(new TradeOrder()))
                .execute();
    }

    public Object async3(String requestId) {
        log.info(requestId + " - async3开始执行 - " + Thread.currentThread().getId());
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3);
        executor.execute(() -> {
            TransactionStatus transaction = this.saveUser();
            try {
                log.info(requestId + " - 到达屏障处，等待其他线程执行...");
                cyclicBarrier.await(5, TimeUnit.SECONDS);
                log.info(requestId + " - 等待结束，准备提交事务！！！");
                transactionManager.commit(transaction);
            } catch (Exception e) {
                e.printStackTrace();
                log.info(requestId + " - 等待结束，发生异常，准备回滚事务！！！");
                transactionManager.rollback(transaction);
            }
        });
        executor.execute(() -> {
            TransactionStatus transaction = saveGoods();
            try {
                log.info(requestId + " - 到达屏障处，等待其他线程执行...");
                cyclicBarrier.await(5, TimeUnit.SECONDS);
                log.info(requestId + " - 等待结束，准备提交事务！！！");
                transactionManager.commit(transaction);
            } catch (Exception e) {
                e.printStackTrace();
                log.info(requestId + " - 等待结束，发生异常，准备回滚事务！！！");
                transactionManager.rollback(transaction);
            }
        });
        executor.execute(() -> {
            TransactionStatus transaction = saveOrder();
            try {
                log.info(requestId + " - 到达屏障处，等待其他线程执行...");
                cyclicBarrier.await(5, TimeUnit.SECONDS);
                log.info(requestId + " - 等待结束，准备提交事务！！！");
                transactionManager.commit(transaction);
            } catch (Exception e) {
                e.printStackTrace();
                log.info(requestId + " - 等待结束，发生异常，准备回滚事务！！！");
                transactionManager.rollback(transaction);
            }
        });
        log.info(requestId + " - 该次请求已完成！！！！！！！！！！！！！！！！！！！！！！！！！！");
        return 1;
    }

    public Object sync(){
        userMapper.insert(new User());
        goodsMapper.insert(new Goods());
        orderMapper.insert(new TradeOrder());
        return 1;
    }

    public TransactionStatus saveUser(){
        var definition = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        var transaction = transactionManager.getTransaction(definition);
        userMapper.insert(new User());
        return transaction;
    }

    public TransactionStatus saveGoods(){
        var definition = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        var transaction = transactionManager.getTransaction(definition);
        goodsMapper.insert(new Goods());
        return transaction;
    }

    public TransactionStatus saveOrder(){
        var definition = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        var transaction = transactionManager.getTransaction(definition);
        orderMapper.insert(new TradeOrder());
        return transaction;
    }

}
