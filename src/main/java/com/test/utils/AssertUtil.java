package com.test.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;


/**
 * @description 通用业务异常校验工具
 * @author  kang
 */
public class AssertUtil {

    private static final ValidatorPredicate ASSERT_FUNCTION = ValidatorPredicate.identity();

    public static void isTrue(Boolean expression, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(expression);
    }

    public static void isFalse(Boolean expression, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(!expression);
    }

    public static void equels(Object val1, Object val2, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(val1 == val2 || (val1 != null && val1.equals(val2)));
    }

    public static void notEquels(Object val1, Object val2, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(val1 == null || !val1.equals(val2));
    }

    public static void isNull(Object t, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(t == null);
    }

    public static void isNotNull(Object t, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(t != null);
    }

    public static void isEmpty(Collection collection, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(collection == null || collection.isEmpty());
    }

    public static void isEmpty(Map map, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(map == null || map.isEmpty());
    }

    public static void isEmpty(Object[] arr, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(arr == null || arr.length == 0);
    }

    public static void isBlank(String val, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(StringUtils.isBlank(val));
    }

    public static void notContains(Object[] arr, Object val, String errorMsg, Object... params){
        ASSERT_FUNCTION.andThen(errorMsg, params).apply(!Arrays.stream(arr).anyMatch(item -> item.equals(val)));
    }

}
