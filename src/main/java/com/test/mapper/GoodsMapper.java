package com.test.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.domain.Goods;

/**
 * @Description
 * @Author test
 * @Date 2021/4/19
 */

public interface GoodsMapper extends BaseMapper<Goods> {


}
