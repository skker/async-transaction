package com.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.domain.TradeOrder;

/**
 * @Description
 * @Author test
 * @Date 2021/4/19
 */

public interface OrderMapper extends BaseMapper<TradeOrder> {


}
