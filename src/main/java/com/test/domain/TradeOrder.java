package com.test.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @Description  订单表
 * @Author kang
 * @Date 2021/6/17
 */
@Data
public class TradeOrder {

    @TableId(type = IdType.AUTO)
    private Long id;

    private LocalDateTime time;

    private String orderNumber;

    public TradeOrder(){
        this.time = LocalDateTime.now();
        this.orderNumber = UUID.randomUUID().toString();
    }

}
