package com.test.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;


/**
 * @Description 用户表
 * @Author kang
 * @Date 2021/6/17
 */
@Data
public class User {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private String email;

    public User (){
        this.name = "小王";
        this.email = "123@qq.com";
    }

}
