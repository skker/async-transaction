package com.test.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description
 * @Author kang
 * @Date 2021/6/17
 */
@Data
public class Goods {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private BigDecimal price;

    public Goods (){
        this.name = "iphone";
        this.price = new BigDecimal(8000);
    }

}
